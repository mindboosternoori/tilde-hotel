<!doctype html>
<html>
  <head>
    <title>~hotel</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="casas-02.css">
    <link rel="icon" href="../../favicon.png" type="image/x-icon">
  </head>
  <?php
    $weather = json_decode(file_get_contents("/home/rlafuente/public_html/porto-weather.json"), true);

    // É de dia ou de noite?
    $sunrise = $weather['sys']['sunrise'];
    $sunset = $weather['sys']['sunset'];
    $now = new DateTime();
    $now = $now->getTimestamp();
    if ($now > $sunrise && $now < $sunset) {
      echo '<body class="dia">';
    } else {
      echo '<body class="noite">';
    }

    // Ir buscar o ícone do estado do tempo ao OWM
    $icon = $weather['weather']['0']['icon'];
    echo '<div id="weather-icon" style="background-image: url(http://openweathermap.org/img/wn/' . $icon . '@2x.png);"></div>';
  ?>
    <div id="page-wrapper">
      <header>
        <h1><a href="../../"><strong>~</strong> Hotel</a></h1>
      </header>
      <div class="main">
        
        <?php
          // aqui fazemos a lista de utilizadores e outra com os que
          // estão online

          // obter os utilizadores do sistema listando o /home
          $users = scandir('/home');
          // remover diretórios que não interessam para a lista
          $users = array_diff($users, array('.', '..', 'pi'));

          // para os users online, vamos fazer um array novo e ir buscá-los
          $online_users = [];
          // correr o comando "who" e capturar o resultado
          exec("who", $output_lines);
          // filtrar as linhas para obtermos a lista de users
          foreach ($output_lines as $line) {
            array_push($online_users, explode(" ", $line)[0]);
          }
          // há muitos repetidos, por isso retiramo-los
          $online_users = array_unique($online_users);
        ?>

        <!-- Aqui é que fazemos tudo acontecer -->
        <ul id="hotel">
          <?php
            foreach ($users as $user) {
              if (in_array($user, $online_users)) {
                echo '<li class="online user"><a href="/~' . $user . '"><span data-tooltip="' . $user . '">' . $user . '</span></a></li>';
              } else {
                echo '<li class="user"><a href="/~' . $user . '"><span data-tooltip="' . $user . '">' . $user . '</span></a></li>';
              }
            }
          ?>
        </ul>
        <p id="rececao"><span>receção</span></p>

      </div><!-- /.main -->
    </div><!-- /#page-wrapper -->

  </body>
</html>

